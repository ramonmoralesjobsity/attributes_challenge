##Installation
1. `cd /path/to/project`
2. `npm install`
3. `npm start`


##Specifications
Create a form that can manage an attributes list and displays the output in JSON format live.
The form will have a fake save button that will get disabled if:
```
#!
 1. One of the attributes name is duplicated among them.
 2. One of the attribute component is invalid (more info below).
```
Add tabs to the form and distribute the attributes in a given number of
categories

```
#!
• Categories are static and only to allow the user to easily recognize the attributes.
```

Internally all the attributes belong to the same list but each one should have a property that allows the tab to render attributes, which belong to the tab’s category.
Have an add attribute button to dynamically add attributes in the current selected category.
Each attribute can be removed with a delete icon within the component.
The overall design is not required, use a front end framework to do it

The attribute has all the following properties:

```
#!

• Name: is required and has to be Unique across all the attributes
• Description
• Device Resource Type: always disabled on this exercise.
• Default Value
• Data Type
• Format
• The rest continues depending on format/data­type values.
```

Data Type: String

```
#!
­ Formats: None, Number, Boolean, Date­Time, CDATA and URI

```

```
#!

When “none” is selected it activates an enumerated list of strings:
• Have the enumerations input able to add values to the attribute
• Prevent empty enumerated values from being added to the list
```

```
#!

When “number” is selected it activates a ranged set of values:
• Range min/max has to be valid (min cant be gt max, max can be lt min) and these can be either float or integer values. Both are required.
• Precision is the step factor between the defined ranges; its value has to allow the user to go through min to max and not exceed it. Required.
• Accuracy acts the same than Precision.
```

When “Boolean”, “Data­type”, “URL” there’s no custom behavior:

Data Type: Object


```
#!

• Default Value is disabled
• Format is disabled.
```

The attribute object has to always reflect what is displayed in a given combination (do not keep previous combination values in the object, make them null)
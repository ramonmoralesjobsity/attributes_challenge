import './App.css';
import Categories from './containers/category/categories';
import Col from 'muicss/lib/react/col';
import Container from 'muicss/lib/react/container';
import React, { Component } from 'react';
import Row from 'muicss/lib/react/row';

class App extends Component {
  render(){
    return (
      <Container fluid={true}>
        <Row>
          <Col xs="12" md="12">
            <Categories/>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;

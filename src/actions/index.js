const Actions =  {
  addAttribute: (attribute) => {
    return {
      type: "ADD_ATTRIBUTE",
      payload: attribute
    }
  },
  updateAttribute: (attribute) => {
    return {
      type: "UPDATE_ATTRIBUTE",
      payload: attribute
    }
  },
  deleteAttribute: (attribute) => {
    return {
      type: "DELETE_ATTRIBUTE",
      payload: attribute
    }
  }
}

export default Actions;

import _ from 'lodash';

export default {
  isValidSaveButton: (attributes) => {
    let isEmptyName = false;
    let isValidRange = true;
    const names = _.map(attributes, 'name');

    const isDuplicate = _.filter(names, (value, index, iterate) => {
        if(!value.length && !isEmptyName) isEmptyName = true;
        return _.includes(iterate, value, index + 1);
      }).length > 0;

    attributes.forEach((a) => {
      if(a.dataType === 'String' && a.format === 'Number'){
        if(!(_.gt(Number(a.rangeMax), Number(a.rangeMin))
          && a.rangeMax
          && a.rangeMin)
          || !isValidFactor(Number(a.rangeMin), Number(a.rangeMax), Number(a.precision))
          || !isValidFactor(Number(a.rangeMin), Number(a.rangeMax), Number(a.accuracy))){
          isValidRange = false;
          return;
        }
      }
    } )

    return !isDuplicate && !isEmptyName && attributes.length && isValidRange;
  }
}

const isValidFactor = (rangeMin, rangeMax, value) => {
  if(!rangeMin || !rangeMax) return false;
  const rangeDiff = (Number(rangeMax) - Number(rangeMin));

  return Number(rangeDiff) % Number(value) === 0;
}

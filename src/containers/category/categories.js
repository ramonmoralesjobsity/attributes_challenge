import React, { Component } from 'react';
import { connect } from 'react-redux';

import Tab from 'muicss/lib/react/tab';
import Tabs from 'muicss/lib/react/tabs';
import Attributes from '../attribute/attributes';
import '../../App.css';


class Category extends Component {
  getTabs(tabs){
    return tabs.map((category) => {
      return <Tab key={category} value={category} label={category}>
        <Attributes category = {category}/>
      </Tab>
    } )
  }

  render(){
    return (
      <div>
        <Tabs onChange={this.onChange} defaultSelectedIndex={0} justified={true}>
          { this.getTabs(this.props.categories) }
        </Tabs>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    categories: state.categories,
  }
}

export default connect(mapStateToProps)(Category);


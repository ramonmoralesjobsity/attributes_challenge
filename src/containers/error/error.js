import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Error extends Component {
  render(){
    return (
      <div style={{marginBottom: '10px'}}>
        <span style={{color: '#D84315', marginBottom: '5px'}}>{this.props.msg}</span>
      </div>
    )
  }
}

Error.propTypes = {
  name: PropTypes.string
};

Error.defaultProps = {
  msg: 'Error...'
};

export default Error;

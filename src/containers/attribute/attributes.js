import React, {Component} from 'react';
import {connect} from 'react-redux';
import AttributeItem from './attribute_item';
import PropTypes from 'prop-types';
import Col from 'muicss/lib/react/col';
import Row from 'muicss/lib/react/row';
import Button from 'muicss/lib/react/button'
import { bindActionCreators }  from 'redux';
import Actions  from '../../actions/index';
import validations from '../../helpers/validations';

let attributeId = 1;

class Attributes extends Component {
  getAttributesByCategory() {
    const category = this.props.category;
    return this.props.attributes.filter((attribute) => {
      return attribute.category === category;
    })
  }

  duplicatedName(attributeName) {
    return this.props.attributes.filter((attribute) => {
        return attribute.name === attributeName
      }).length > 1
  }

  handleAddAttribute(){
    const defaultAttribute =  {
      id: attributeId++,
      name: '',
      description: '',
      deviceResourceType: '',
      defaultValue: '',
      dataType: 'String',
      format: 'None',
      enumerations: [],
      rangeMin: null,
      rangeMax: null,
      unitOfMeasurement: null,
      precision: null,
      accuracy: null,
      category: this.props.category
    }
    this.props.addAttribute(defaultAttribute);
  }

  isValidSaveButton(){
    return validations.isValidSaveButton(this.props.attributes);
  }

  render() {
    const attributesJSON = JSON.stringify(this.props.attributes, null, 4);
    return (
      <Row>
        <Col md={7}>
          { this.getAttributesByCategory().map((attribute) => {
            return <AttributeItem
              key={attribute.id}
              attribute={attribute}
              duplicatedName={this.duplicatedName.bind(this)}
            />
          }) }
          <Button color="primary" onClick={this.handleAddAttribute.bind(this)}>
            Add Attribute
          </Button>
          <Button color="primary" disabled={!this.isValidSaveButton()} >Save</Button>
        </Col>
        <Col md={5}>
          <pre>
            <div style={{backgroundColor: '#D3D3D3'}}>
              <code>
              {attributesJSON}
            </code>
            </div>
          </pre>
        </Col>
      </Row>
    )
  }
}

Attributes.propTypes = {
  category: PropTypes.string.isRequired
};

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
      addAttribute: Actions.addAttribute,
      deleteAttribute: Actions.deleteAttribute
  }, dispatch);
}

function mapStateToProps(state) {
  return {
    attributes: state.attributes
  }
}

export default connect(mapStateToProps, matchDispatchToProps)(Attributes);


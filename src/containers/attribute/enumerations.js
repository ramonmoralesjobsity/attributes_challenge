import React, { Component } from 'react';
import Button from 'muicss/lib/react/button';
import Col from 'muicss/lib/react/col';
import Input from 'muicss/lib/react/input';
import Row from 'muicss/lib/react/row';

class Enumerations extends Component {
  constructor(){
    super();
    this.state = {
      enumeration: '',
      enumerations: [],
      currentEnumeration:''
    }
  }

  handleOnClick(e){
    e.preventDefault();
    this.setState({
      enumerations: [...this.state.enumerations, this.state.enumeration]
    },() => {
        this.updateEnumerations();
    })
  }

  updateEnumerations(){
    const attribute = {
      ...this.props.attribute,
      enumerations: this.state.enumerations
    };
    this.props.updateAttribute(attribute);
    this.setState({
      enumeration: ''
    })
  }

  handleOnChange(e){
    this.setState({
      enumeration: e.target.value
    })
  }

  displayEnumerations(){
    return (
      <ul>
        {this.state.enumerations.map((enumeration, key) => {
          return <li
            style={{listStyleType: 'none'}}
            key={key}>
            <Button
              key={ 'b' + key }
              size="small"
              color="danger"
              variant="fab"
              onClick={(e) => {
                e.preventDefault();
                this.setState({
                  enumerations: [
                    ...this.state.enumerations.slice(0, key),
                    ...this.state.enumerations.slice(key + 1)
                  ]
                }, () => {
                  this.updateEnumerations();
                })
              } }
            >
               -
            </Button>
              &nbsp;&nbsp; {enumeration}
            </li>
        } ) }
      </ul>
    )
  }

  render(){
    return (
      <Row>
        <Col md= {5} >
          <div className="mui-form--inline">
            <Input label="Enumerations"
                   value={this.state.enumeration}
                   required={true}
                   onChange={this.handleOnChange.bind(this)}
            />
            <Button
              color="primary"
              onClick={ this.handleOnClick.bind(this)}
              disabled={!this.state.enumeration.length}>Add</Button>
          </div>
        </Col>
        <Col md= {7}>
          {this.displayEnumerations()}
        </Col>
      </Row>
    )
  }
}

export default Enumerations;

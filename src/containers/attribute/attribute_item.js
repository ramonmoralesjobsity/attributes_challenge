import React, { Component } from 'react';
import Col from 'muicss/lib/react/col';
import { connect } from 'react-redux';
import Error from '../error/error';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Option from 'muicss/lib/react/option';
import PropTypes from 'prop-types';
import Row from 'muicss/lib/react/row';
import Select from 'muicss/lib/react/select';
import Actions  from '../../actions/index';
import { bindActionCreators }  from 'redux';
import Divider from 'muicss/lib/react/divider';
import Button from 'muicss/lib/react/button';
import Enumerations from "./enumerations";
import Range from "./range";

class AttributeItem extends Component{
  constructor(props){
    super(props);
    this.state = {
      attribute: props.attribute,
      showDetails: false
    };
  }

  showDuplicatedError(name){
    if(this.props.duplicatedName(name)){
      return <Error msg="Attribute 's name has to be unique (*)"/>
    }
  }

  handleOnChangeInput(e){
    let attribute = {...this.state.attribute};
    attribute[e.target.name] = e.target.value;

    if(e.target.value === 'Number'){
      attribute = {
        ...attribute,
        rangeMin: 0,
        rangeMax: 0,
        unitOfMeasurement: '',
        precision: 0,
        accuracy: 0
      }
    }else{
      attribute = {
        ...attribute,
        rangeMin: null,
        rangeMax: null,
        unitOfMeasurement: null,
        precision: null,
        accuracy: null
      }
    }

    this.setState({attribute: attribute},() => {
      this.props.updateAttribute(this.state.attribute);
    })
  }

  handleOnClickDeleteAttribute(){
    this.props.deleteAttribute(this.state.attribute);
  }

  getElementsByDataTypeFormat(){
    if(this.state.attribute.dataType === 'String'){
      switch (this.state.attribute.format){
        case 'None':
          return <Enumerations updateAttribute={this.props.updateAttribute} attribute={this.state.attribute}/>;
        case 'Number':
          return <Range updateAttribute={this.props.updateAttribute} attribute={this.state.attribute}/>
        default:
          return '';
      }
    }
  }

  isDataTypeObject(){
    return this.state.attribute.dataType === 'Object';
  }

  displayFormats(){
    if(this.isDataTypeObject()){
      return <Input label="Format" disabled={true}/>
    }else{
      return <Select name="format"
                     label="Format"
                     onChange={this.handleOnChangeInput.bind(this)}>
        { this.props.formats.map((format) => {
          return <Option
            key= {format}
            value={format}
            label={format}
          />
        })}
      </Select>
    }
  }

  toggleDetails(e){
    e.preventDefault();
    this.setState({
      showDetails: !this.state.showDetails
    })
  }

  displayDetailsLabel(){
    return this.state.showDetails ? '↑' : '↓';
  }

  displayAttributeDetails(){
    if(this.state.showDetails){
      return (
        <Row>
          <Col md={ 6 } >
            <Input label="Device Resource Type" floatingLabel={true} disabled={ true } />
            <Select name="dataType" label="Data Type" defaultValue={ 'String' } onChange={ this.handleOnChangeInput.bind(this) }>
              { this.props.dataTypes.map((dataType) => {
                return <Option
                  key= {dataType}
                  value={dataType}
                  label={dataType}
                />
              })}
            </Select>
          </Col>
          <Col md={ 6 } >
            <Input name="defaultValue" label="Default Value" floatingLabel={true} onChange={this.handleOnChangeInput.bind(this)} disabled={this.isDataTypeObject()} />
            {this.displayFormats()}
          </Col>
          <Col md={ 12 }>
            {this.getElementsByDataTypeFormat()}
          </Col>
        </Row>)
    }
  }

  render(){
    return (
        <Form>
          <Row>
            <Col md={12}>
              <Button variant="fab" color="primary" className="mui--pull-right" onClick={this.toggleDetails.bind(this)}>{ this.displayDetailsLabel()}</Button>
            </Col>
          </Row>
          <Input name="name" value={this.state.attribute.name}  label="Name" floatingLabel={true} required={true} onChange={this.handleOnChangeInput.bind(this)}/>
          { this.showDuplicatedError( this.state.attribute.name ) }
          <Input name="description" label="Description" floatingLabel={true}  onChange={this.handleOnChangeInput.bind(this)}/>
          {this.displayAttributeDetails()}
          <Row>
            <Col md={12}>
              <div className="mui--pull-right">
                <Button color="danger" onClick={this.handleOnClickDeleteAttribute.bind(this)}>Delete</Button>
              </div>
            </Col>
          </Row>
          <Divider/>
        </Form>
    )
  }
}

AttributeItem.propTypes = {
  attribute: PropTypes.object.isRequired
};

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
      updateAttribute: Actions.updateAttribute,
      deleteAttribute: Actions.deleteAttribute
  }, dispatch);
}

function mapStateToProps(state) {
  return {
    dataTypes: state.dataTypes,
    formats: state.formats
  }
}

export default connect(mapStateToProps ,matchDispatchToProps)(AttributeItem);

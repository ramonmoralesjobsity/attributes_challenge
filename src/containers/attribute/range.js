import React, { Component } from 'react';
import Col from 'muicss/lib/react/col';
import Input from 'muicss/lib/react/input';
import Row from 'muicss/lib/react/row';
import Error from '../error/error';

class Range extends Component{
  constructor(props){
    super(props);
    this.state = {
      rangeMin: 0,
      rangeMax: 0,
      unitOfMeasurement: '',
      precision: 0,
      accuracy: 0
    }
  }

  handleOnChangeInput(e){
    const attribute = this.props.attribute;
    attribute[e.target.name] = e.target.value;
    this.setState(attribute, () => {
      this.props.updateAttribute(attribute);
    })
  }
  
  isValidRange(){
    return (Number(this.state.rangeMax) > Number(this.state.rangeMin))
      && this.state.rangeMax
      && this.state.rangeMin;
  }

  isValidFactor(value){
    if(!this.state.rangeMin || !this.state.rangeMax || !this.isValidRange()) return false;
    var rangeDiff = (Number(this.state.rangeMax) - Number(this.state.rangeMin));

    return Number(rangeDiff) % Number(value) === 0;
  }

  displayValidRangeError(){
    if(!this.isValidRange()){
      return <Error msg="Range Min has to be less than Range Max (*)" />
    }
  }

  displayValidFactorError(value){
    if(!this.isValidFactor(value)){
      return <Error msg="The value has to go through min to max and not exceed it. (*)" />
    }
  }
  render(){
    return (
      <div>
        <Row>
          <Col md="6">
            <Input name="rangeMin"
                   type="Number"
                   value={this.state.rangeMin}
                   label="Range Min" floatingLabel={true}
                   onChange={this.handleOnChangeInput.bind(this)}
                   required={true}
            />
            {this.displayValidRangeError()}
          </Col>
          <Col md="6">
            <Input name="rangeMax"
                   type="Number"
                   value={ this.state.rangeMax}
                   label="Range Max"
                   floatingLabel={true}
                   onChange={this.handleOnChangeInput.bind(this)}
                   required={true}
            />
          </Col>
        </Row>

        <Row>
          <Col md="4">
            <Input name="unitOfMeasurement"
                   value={this.state.unitOfMeasurement}
                   label="Unit of Measurement"
                   floatingLabel={true}
                   onChange={this.handleOnChangeInput.bind(this)}
                   required={true}/>
          </Col>
          <Col md="4">
            <Input name="precision"
                   type="Number"
                   value={this.state.precision}
                   label="Precision"
                   floatingLabel={true}
                   onChange={this.handleOnChangeInput.bind(this)}
                   required={true}
            />
            {this.displayValidFactorError(this.state.precision)}
          </Col>
          <Col md="4">
            <Input name="accuracy"
                   type="Number"
                   value={this.state.accuracy}
                   label="Accuracy"
                   floatingLabel={true}
                   onChange={this.handleOnChangeInput.bind(this)}
                   required={true}/>
            {this.displayValidFactorError(this.state.accuracy)}
          </Col>
        </Row>
      </div>
    )
  }
}
export default Range;
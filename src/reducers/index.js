import { combineReducers } from  'redux';
import AttributesReducer from './reducer_attributes';
import CategoriesReducer from './reducer_categories';
import DataTypesReducer from './reducer_data_types';
import FormatReducer from './reducer_formats';

const allReducers  =  combineReducers({
  attributes: AttributesReducer,
  categories: CategoriesReducer,
  dataTypes: DataTypesReducer,
  formats: FormatReducer
})

export default allReducers;
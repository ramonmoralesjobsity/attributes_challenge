export default (state = [], action) => {
  switch (action.type){
    case 'ADD_ATTRIBUTE':
      return [...state, action.payload]
    case 'UPDATE_ATTRIBUTE':
      return state.map( (attribute) => {
        if(attribute.id === action.payload.id){
          return action.payload;
        }
        return attribute;
      })
    case 'DELETE_ATTRIBUTE':
      return state.filter( (attribute) => {
        return (attribute.id !== action.payload.id)
      })
    default:
      return state;
  }
}
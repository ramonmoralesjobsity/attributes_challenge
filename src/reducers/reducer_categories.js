export default () => {
  return [
    'Device Info',
    'Sensors',
    'Settings',
    'Commands',
    'Metadata'
  ]
}
export default () => {
  return [
    'None',
    'Number',
    'Boolean',
    'Date­Time',
    'CDATA',
    'URI',
  ]
}